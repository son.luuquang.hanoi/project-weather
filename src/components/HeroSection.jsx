import React from "react";
import "../App.scss";
// import { Button } from "./Button";
import "./HeroSection.scss";
import { useState } from 'react';
import apiWeather from './apiWeather';

const HeroSection = () => {
  const [query, setquery] = useState('');
  const [weather, setweather] = useState({});
  const search = async (e) => {
    if(e.key === 'Enter') {
      const data = await apiWeather(query);
      setweather(data);
      setquery('');
    }
  }
  return (
    <div className="hero-container">
      <video src="/videos/video-1.mp4" autoPlay loop muted />
      <h1>My Weather</h1>
      <h2>Today , What is the weather like in your place ? </h2>
      <div className="hero-btns">

      <input type="text" className="form-control"  placeholder="Find Location..." 
                  value={query}onChange={(e)=>setquery(e.target.value)}onKeyPress={search}/>
      {weather.main && (
                <div className="city">
                    <h2 className="city-name">
                        <span>{weather.name}</span>
                        <sup>{weather.sys.country}</sup>
                    </h2>
                    <div className="city-temp">
                        {Math.round(weather.main.temp)}
                        <sup>&deg;C</sup>
                    </div>
                    <div className="info">
                        <img className="city-icon" src={`https://openweathermap.org/img/wn/${weather.weather[0].icon}@2x.png`} alt={weather.weather[0].description} />
                        <p>{weather.weather[0].description}</p>
                    </div>
                </div>
            )}
      </div>
    </div>
  );
};

export default HeroSection;
