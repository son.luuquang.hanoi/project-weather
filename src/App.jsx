import React, { Component} from 'react';
import Navbar from './components/Navbar';
import './App.scss';
import Home from './components/pages/Home';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Services from './components/pages/Services';
import Map from './components/pages/Map';
import SignIn from './components/pages/SignIn';


class App extends Component {
  render () {
    return (
      <>
        <Router>
          <Navbar />
          <Switch>
            <Route path='/' exact component={Home} />
            <Route path='/services' component={Services} />
            <Route path='/maps' component={Map} />
            <Route path='/sign-in' component={SignIn} />
          </Switch>
        </Router>
      </>
    );
  }
}

export default App;
